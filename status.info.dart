import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    home: StatusInfo(),
  ));
}

class StatusInfo extends StatefulWidget {
  const StatusInfo({Key key}) : super(key: key);

  @override
  _StatusInfoState createState() => _StatusInfoState();
}

class _StatusInfoState extends State<StatusInfo> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
        child: Column(
        children: [
        Container(
        height: 190,
        width: double.infinity,
        decoration: BoxDecoration(
        color: Colors.tealAccent[700],
        boxShadow: [
        BoxShadow(color: Colors.grey),
    ],
    borderRadius: BorderRadius.vertical(
    bottom: Radius.elliptical(
    MediaQuery.of(context).size.width, 250.0))),
    child: Column(
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
    Align(
    alignment: Alignment.bottomCenter,
    child: Text(
    "Status info",
    textAlign: TextAlign.center,
    style: TextStyle(
    color: Colors.white,
    fontSize: 30,
    fontWeight: FontWeight.bold),
    ),
    ),

    ],
    ),
    ),
          SizedBox(
            height: 20,),
          Container(
            width: 400,
            height: 40,
            child: Row(
              children: [
                Icon(Icons.add_alert,color: Colors.tealAccent[700],),
                Text("Support Alert"),

              ],
            ),
            decoration: BoxDecoration(
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 1,
                  blurRadius: 5,
                  offset: Offset(0, 4),
                ),
              ],
              borderRadius: BorderRadius.all(Radius.circular(1)),
            ),
            padding: EdgeInsets.all(2.0),

          ),
          Container(
            width: 400,
            height: 40,
            child: Row(
              children: [

                Text("Abram Danny"),
                SizedBox(
                  width: 180,),

                Text("-Tap to connect",style: TextStyle(
                  color: Colors.red,fontWeight: FontWeight.bold
                ),),

              ],
            ),
            decoration: BoxDecoration(
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 1,
                  blurRadius: 5,
                  offset: Offset(0, 4),
                ),
              ],
              borderRadius: BorderRadius.all(Radius.circular(1)),
            ),
            padding: EdgeInsets.all(2.0),

          ),
          Container(
            width: 400,
            height: 40,
            child: Row(
              children: [

                Text("Davidson"),
                SizedBox(
                  width: 205,),

                Text("-Tap to connect",style: TextStyle(
                    color: Colors.red,fontWeight: FontWeight.bold
                ),),

              ],
            ),
            decoration: BoxDecoration(
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 1,
                  blurRadius: 5,
                  offset: Offset(0, 4),
                ),
              ],
              borderRadius: BorderRadius.all(Radius.circular(1)),
            ),
            padding: EdgeInsets.all(2.0),

          ),

          SizedBox(
            height: 20,),
          Container(
            width: 400,
            height: 40,
            child: Row(
              children: [
                Icon(Icons.add_alert,color: Colors.tealAccent[700]),
                Text("Sky Blue Pending"),

              ],
            ),
            decoration: BoxDecoration(
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 1,
                  blurRadius: 5,
                  offset: Offset(0, 4),
                ),
              ],
              borderRadius: BorderRadius.all(Radius.circular(1)),
            ),
            padding: EdgeInsets.all(2.0),

          ),
          Container(
            width: 400,
            height: 40,
            child: Row(
              children: [

                Text("Jeyanthy"),
                SizedBox(
                  width: 205,),

                Text("-Tap to connect",style: TextStyle(
                    color: Colors.red,fontWeight: FontWeight.bold
                ),),

              ],
            ),
            decoration: BoxDecoration(
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 1,
                  blurRadius: 5,
                  offset: Offset(0, 4),
                ),
              ],
              borderRadius: BorderRadius.all(Radius.circular(1)),
            ),
            padding: EdgeInsets.all(2.0),

          ),
          Container(
            width: 400,
            height: 40,
            child: Row(
              children: [

                Text("David"),
                SizedBox(
                  width: 225,),

                Text("-Tap to connect",style: TextStyle(
                    color: Colors.red,fontWeight: FontWeight.bold
                ),),

              ],
            ),
            decoration: BoxDecoration(
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 1,
                  blurRadius: 5,
                  offset: Offset(0, 4),
                ),
              ],
              borderRadius: BorderRadius.all(Radius.circular(1)),
            ),
            padding: EdgeInsets.all(2.0),

          ),
          SizedBox(
            height: 20,),
            Container(
              width: 400,
              height: 40,
              child: Row(
                children: [
                  Icon(Icons.add_alert,color: Colors.tealAccent[700]),
                  Text("Sky Blue "),

                ],
              ),
              decoration: BoxDecoration(
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    spreadRadius: 1,
                    blurRadius: 5,
                    offset: Offset(0, 4),
                  ),
                ],
                borderRadius: BorderRadius.all(Radius.circular(1)),
              ),
              padding: EdgeInsets.all(2.0),

            ),

          Container(
            width: 400,
            height: 40,
            child: Row(
              children: [

                Text("Evana"),
                SizedBox(
                  width: 225,),

                Text("-5:00 Pm",style: TextStyle(
                    color: Colors.red,fontWeight: FontWeight.bold
                ),),

              ],
            ),
            decoration: BoxDecoration(
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 1,
                  blurRadius: 5,
                  offset: Offset(0, 4),
                ),
              ],
              borderRadius: BorderRadius.all(Radius.circular(1)),
            ),
            padding: EdgeInsets.all(2.0),

          ),
          Container(
            width: 400,
            height: 40,
            child: Row(
              children: [

                Text("Keziah"),
                SizedBox(
                  width: 220,),

                Text("-4:00 Pm",style: TextStyle(
                    color: Colors.red,fontWeight: FontWeight.bold
                ),),

              ],
            ),
            decoration: BoxDecoration(
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 1,
                  blurRadius: 5,
                  offset: Offset(0, 4),
                ),
              ],
              borderRadius: BorderRadius.all(Radius.circular(1)),
            ),
            padding: EdgeInsets.all(2.0),

          ),
    ]

    )
    ));
  }
}
